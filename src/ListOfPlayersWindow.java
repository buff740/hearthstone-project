import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ListOfPlayersWindow extends JFrame implements ActionListener {
    private JTextArea textArea;
    private JScrollPane scrollPane;
    private Player[] playerArr;
    private OpeningWindow parentFrame;
    private JButton ok;

    ListOfPlayersWindow(OpeningWindow parentFrame, Player[] playerArr){
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setSize(300,800);
        setLayout(new BorderLayout());

        this.parentFrame = parentFrame;
        parentFrame.setEnabled(false);

        this.playerArr = playerArr;

        ok = new JButton("OK");
        ok.addActionListener(this);
        textArea = new JTextArea();
        textArea.setEditable(false);
        scrollPane = new JScrollPane(textArea);
        scrollPane.createVerticalScrollBar();

        add(scrollPane, BorderLayout.CENTER);
        add(ok, BorderLayout.SOUTH);

        fillTextArea();

    }

    public void fillTextArea(){
        for(int I = 1; I<parentFrame.getPlayersNo()+1; I++){
            textArea.append("["+I+"] = "+playerArr[I-1].toString());
            textArea.append("\n");
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        parentFrame.setEnabled(true);
        this.dispose();
    }
}
