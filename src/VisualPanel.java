import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class VisualPanel extends JPanel {
    Graphics g;
    BufferedImage image;

    VisualPanel(){
        g = getGraphics();
        try {
            image = ImageIO.read(ImagePanel.class.getResource("/images/pvphs.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.drawImage(image,0,0,720,169,null);
    }

}
