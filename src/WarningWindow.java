import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class WarningWindow extends JFrame implements ActionListener{
    private JLabel jl = new JLabel();
    private JButton ok = new JButton("Ok");
    private JFrame parentFrame;

    WarningWindow(String message, JFrame parentFrame){

        this.parentFrame = parentFrame;
        setSize(300,100);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setLayout(new GridLayout(2,1));

        jl.setText(message);
        jl.setHorizontalAlignment(JLabel.CENTER);
        add(jl);

        add(ok);
        ok.addActionListener(this);
        parentFrame.setEnabled(false);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        parentFrame.setEnabled(true);
        this.dispose();
    }
}