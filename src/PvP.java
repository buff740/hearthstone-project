import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PvP extends JPanel implements ActionListener {
    Player p1,p2;
    JPanel visual,score;
    JButton p1p,p1m,p2p,p2m;
    JLabel names;
    JLabel scoreLabel;
    int P1VISUALSCORE=0,P2VISUALSCORE=0;
    int P1SCORE, P2SCORE;
    int P1S, P2S;

    PvP(Player p1, Player p2){
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));

        p1p = new JButton("+");
        p1p.setFocusPainted(false);
        p1p.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));
        p1p.setBackground(Color.DARK_GRAY);
        p1p.addActionListener(this);

        p1m = new JButton("-");
        p1m.setFocusPainted(false);
        p1m.setBackground(Color.DARK_GRAY);
        p1m.addActionListener(this);
        p1m.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));

        p2p = new JButton("+");
        p2p.setFocusPainted(false);
        p2p.setBackground(Color.DARK_GRAY);
        p2p.addActionListener(this);
        p2p.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));

        p2m = new JButton("-");
        p2m.setFocusPainted(false);
        p2m.setBackground(Color.DARK_GRAY);
        p2m.addActionListener(this);
        p2m.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));


        names = new JLabel( p1.toString() + "       X       " + p2.toString());
        names.setHorizontalAlignment(JLabel.CENTER);

        scoreLabel = new JLabel("0-0");
        scoreLabel.setHorizontalAlignment(JLabel.CENTER);

        visual = new VisualPanel();
        visual.setLayout(new GridLayout(2,1));
        visual.add(names);
        visual.add(scoreLabel);


        score = new JPanel();
        score.setLayout(new GridLayout(1,4));
        score.add(p1p);
        score.add(p1m);
        score.add(p2p);
        score.add(p2m);

        this.p1 = p1;
        this.p2 = p2;


        add(visual,BorderLayout.CENTER);
        add(score, BorderLayout.SOUTH);

        names.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));
        scoreLabel.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));

    }

    @Override
    public void actionPerformed(ActionEvent e) {
            if (e.getSource() == p1p) {
                if((P1VISUALSCORE+P2VISUALSCORE)!=5) {
                    P1VISUALSCORE++;
                    scoreLabel.setText(P1VISUALSCORE + "-" + P2VISUALSCORE);
                    P2SCORE = P2VISUALSCORE - P1VISUALSCORE;
                    P1SCORE = P1VISUALSCORE - P2VISUALSCORE;
                }
            } else if (e.getSource() == p1m) {
                if (P1VISUALSCORE != 0) {
                    P1VISUALSCORE--;
                    scoreLabel.setText(P1VISUALSCORE + "-" + P2VISUALSCORE);
                    P2SCORE = P2VISUALSCORE - P1VISUALSCORE;
                    P1SCORE = P1VISUALSCORE - P2VISUALSCORE;
                }
            } else if (e.getSource() == p2p) {
                if((P1VISUALSCORE+P2VISUALSCORE)!=5) {
                    P2VISUALSCORE++;
                    scoreLabel.setText(P1VISUALSCORE + "-" + P2VISUALSCORE);
                    P2SCORE = P2VISUALSCORE - P1VISUALSCORE;
                    P1SCORE = P1VISUALSCORE - P2VISUALSCORE;
                }
            } else if (e.getSource() == p2m) {
                if (P2VISUALSCORE != 0) {
                    P2VISUALSCORE--;
                    scoreLabel.setText(P1VISUALSCORE + "-" + P2VISUALSCORE);
                    P2SCORE = P2VISUALSCORE - P1VISUALSCORE;
                    P1SCORE = P1VISUALSCORE - P2VISUALSCORE;
                }
            }
            p1.setSCORE(P1SCORE);

            p2.setSCORE(P2SCORE);
        }

        public void updateScore(){

            if(P1VISUALSCORE>P2VISUALSCORE){
                p1.GAMESWON+=1;
                p2.GAMESLOST+=1;
            }else if(P1VISUALSCORE<P2VISUALSCORE){
                p2.GAMESWON+=1;
                p1.GAMESLOST+=1;
            }

            P1S = P1SCORE;
            P2S = P2SCORE;

            p1.setPS(P1S);
            p2.setPS(P2S);

            P1SCORE = 0;
            P2SCORE = 0;

            P1VISUALSCORE = 0;
            P2VISUALSCORE = 0;

            scoreLabel.setText(P1VISUALSCORE + "-" + P2VISUALSCORE);

        }

    }

