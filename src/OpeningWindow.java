import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class OpeningWindow extends JFrame implements ActionListener, KeyListener {
    private String[] playersNo = {"32","64"};
    Player[] playerArr64 = new Player[64];
    Player[] playerArr32 = new Player[32];
    private int PLAYERSNO,ROUNDSNO;
    private int TEMP = 0;
    private JButton playersNoOk,roundsNoOk,playerNameOk,reset,start,showList,generate;
    private JComboBox comboBox;
    private JSpinner roundsNo;
    private JLabel playersNoLabel,name;
    private Font labelFont;
    private JTextField playerName;

    OpeningWindow(){
        try {
            // Set System L&F
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        }
        catch (UnsupportedLookAndFeelException e) {
            // handle exception
        }
        catch (ClassNotFoundException e) {
            // handle exception
        }
        catch (InstantiationException e) {
            // handle exception
        }
        catch (IllegalAccessException e) {
            // handle exception
        }


        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setSize(800,600);
        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(1,1,10,1);

        labelFont = new Font(Font.SANS_SERIF,Font.BOLD,50);

        JLabel playersCount = new JLabel("Pocet hracu");
        playersCount.setBounds(100,65,100,35);

        add(playersCount);

        playersNoLabel = new JLabel();
        playersNoLabel.setFont(labelFont);
        playersNoLabel.setBounds(500,300,150,75);

        roundsNo = new JSpinner(spinnerNumberModel);
        roundsNo.setBounds(100,150,100,35);

        comboBox = new JComboBox(playersNo);
        comboBox.setBounds(100,100,100,35);

        playersNoOk = new JButton("OK");
        playersNoOk.setBounds(210,100,80,35);
        playersNoOk.addActionListener(this);

        roundsNoOk = new JButton("OK");
        roundsNoOk.setBounds(210,150,80,35);
        roundsNoOk.addActionListener(this);

        playerNameOk = new JButton("OK");
        playerNameOk.addActionListener(this);
        playerNameOk.setBounds(275,375,80,35);
        playerNameOk.setEnabled(false);

        reset = new JButton("Reset");
        reset.setBounds(510,415,80,35);
        reset.addActionListener(this);

        start = new JButton("Start");
        start.setEnabled(false);
        start.setBounds(510,375,80,35);
        start.addActionListener(this);

        showList = new JButton("List");
        showList.setEnabled(false);
        showList.addActionListener(this);
        showList.setBounds(500,455,100,35);

        name = new JLabel("Jmeno: ");
        name.setBounds(100,375,50,35);

        playerName = new JTextField();
        playerName.setEnabled(false);
        playerName.setBounds(150,375,100,35);
        playerName.addKeyListener(this);

        generate = new JButton("Generate");
        generate.setBounds(510,50,80,35);
        generate.addActionListener(this);

        add(generate);
        add(playersNoOk);
//        add(roundsNoOk);
        add(reset);
        add(comboBox);
//        add(roundsNo);
        add(start);
        add(playersNoLabel);
        add(playerName);
        add(name);
        add(playerNameOk);
        add(showList);
        generate.setVisible(false);
        comboBox.setSelectedIndex(1);
    }

    public int getPlayersNo(){
        return PLAYERSNO;
    }

    public void initializeArray(Player[] playerArr){
        for(int I = 0; I<playerArr.length;I++){
            playerArr[I] = new Player("");
        }
    }

    private void generatePlayers(Player[] playerArr){
        for(int I = 0; I<playerArr.length; I++){
            playerArr[I]=new Player(Integer.toString(I));
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==playersNoOk) {
            showList.setEnabled(true);
            playerName.setEnabled(true);
            playerNameOk.setEnabled(true);
            playersNoOk.setEnabled(false);
            comboBox.setEnabled(false);
            PLAYERSNO = Integer.valueOf(comboBox.getSelectedItem().toString());
            playersNoLabel.setText("0/"+PLAYERSNO);
        }else if(e.getSource()==roundsNoOk){
            if(!comboBox.isEnabled()){
                showList.setEnabled(true);
                playerName.setEnabled(true);
                playerNameOk.setEnabled(true);
            }
            roundsNoOk.setEnabled(false);
            roundsNo.setEnabled(false);
            ROUNDSNO = (int)roundsNo.getValue();
        }else if(e.getSource()==reset){
            if(!roundsNo.isEnabled()||!comboBox.isEnabled()){
                initializeArray(playerArr64);
                initializeArray(playerArr32);
                playersNoLabel.setText("");
                showList.setEnabled(false);
                TEMP = 0;
                GameWindow.times = 0;
                start.setEnabled(false);
                playerNameOk.setEnabled(false);
                playerName.setText("");
                playerName.setEnabled(false);
                roundsNo.setEnabled(true);
                roundsNoOk.setEnabled(true);
                comboBox.setEnabled(true);
                playersNoOk.setEnabled(true);
            }
        }else if(e.getSource()==playerNameOk) {
                if(!isValid(playerName.getText())){
                    new WarningWindow("Name mustn't contain spaces", this).setVisible(true);
                    playerName.setText("");
                }else {
                    if (playerName.getText().equals("NaN") || playerName.getText().equals("Nan") || playerName.getText().equals("naN") || playerName.getText().equals("nan")) {
                        new WarningWindow("Invalid Name! (Can't be \"NaN\")", this).setVisible(true);
                        playerName.setText("");
                    } else {
                        if (!checkForRepetition(playerName.getText())) {
                            if(comboBox.getSelectedIndex()==1) {
                                playerArr64[TEMP] = new Player(playerName.getText());
                            }else{
                                playerArr32[TEMP] = new Player(playerName.getText());
                            }
                            TEMP++;
                            playersNoLabel.setText(TEMP + "/" + PLAYERSNO);
                            playerName.setText("");
                            if (TEMP == PLAYERSNO) {
                                playerNameOk.setEnabled(false);
                                playerName.setEnabled(false);
                                start.setEnabled(true);
                            }
                        } else {
                            new WarningWindow("Name already in the list! / Name is empty!", this).setVisible(true);
                            playerName.setText("");
                        }
                    }
                }
            }else if (e.getSource() == showList) {
                if(comboBox.getSelectedIndex()==1) {
                    new ListOfPlayersWindow(this, playerArr64).setVisible(true);
                }else{
                    new ListOfPlayersWindow(this, playerArr32).setVisible(true);
                }
            }else if(e.getSource() == start){
            setVisible(false);
                TEMP = 0;
                if (comboBox.getSelectedIndex() == 1) {
                     new GameWindow(PLAYERSNO, ROUNDSNO, Arrays.asList(playerArr64)).setVisible(true);
                } else if (comboBox.getSelectedIndex() == 0) {
                     new GameWindow(PLAYERSNO, ROUNDSNO, Arrays.asList(playerArr32)).setVisible(true);
                }
            }else if(e.getSource()==generate){
                start.setEnabled(true);
                showList.setEnabled(true);
                if(comboBox.getSelectedIndex()==1) {
                    generatePlayers(playerArr64);
                }else{
                    generatePlayers(playerArr32);
                }
        }

        }

    private Boolean isValid(String name){
            for(char c : name.toCharArray()){
                if(c==' '){
                    return false;
                }
            }
            return true;
        }

    private Boolean checkForRepetition(String name){
        if(comboBox.getSelectedIndex()==1) {
            for (Player p : playerArr64) {
                if (p.toString().equals(name)) {
                    return true;
                }
            }
        }else {
            for (Player p : playerArr32) {
                if (p.toString().equals(name)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyChar()==KeyEvent.VK_ENTER) {
            if(!isValid(playerName.getText())){
                new WarningWindow("Name mustn't contain spaces", this).setVisible(true);
                playerName.setText("");
            }else {
                if (playerName.getText().equals("NaN") || playerName.getText().equals("Nan") || playerName.getText().equals("naN") || playerName.getText().equals("nan")) {
                    new WarningWindow("Invalid Name! (Can't be \"NaN\")", this).setVisible(true);
                    playerName.setText("");
                } else {
                    if (!checkForRepetition(playerName.getText())) {
                        if(comboBox.getSelectedIndex()==1) {
                            playerArr64[TEMP] = new Player(playerName.getText());
                        }else{
                            playerArr32[TEMP] = new Player(playerName.getText());
                        }
                        TEMP++;
                        playersNoLabel.setText(TEMP + "/" + PLAYERSNO);
                        playerName.setText("");
                        if (TEMP == PLAYERSNO) {
                            playerNameOk.setEnabled(false);
                            playerName.setEnabled(false);
                            start.setEnabled(true);
                        }
                    } else {
                        new WarningWindow("Name already in the list! / Name is empty!", this).setVisible(true);
                        playerName.setText("");
                    }
                }
            }
        }
    }


    @Override
    public void keyReleased(KeyEvent e) {

    }

}
