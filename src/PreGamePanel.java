import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class PreGamePanel extends JPanel {
    private int temp;
    private ArrayList<SortedPanel> sortedPanels;
    public ArrayList<Player> bestPlayers = new ArrayList<>();

    PreGamePanel(int PLAYERSNO, ArrayList <Player> playerArr){

        setLayout(new GridLayout(2,4,20,20));

        sortedPanels = new ArrayList<>();

        int x = PLAYERSNO/8;

        temp = 0;
        for(int I = 0; I<x; I++){
            sortedPanels.add(new SortedPanel(temp, playerArr,8,2,0));
            add(sortedPanels.get(I));
            temp+=8;

        }

    }

    public void printwh(){
        for(SortedPanel s : sortedPanels){
            System.out.println(s.getWidth());
            System.out.println(s.getHeight());
        }
    }

    public void playOff(){
        SortedPanel.playoff = true;
        setLayout(new GridLayout(2,1));
        getBestPlayersArr();
        setVisible(false);
        removeAll();
        if(GameWindow.PLAYERSNO==64){
            add(new SortedPanel(0,bestPlayers,4,2,8));
        }else {
            add(new SortedPanel(0, bestPlayers, 4, 2, 4));
        }
        JLabel jl = new JLabel("PLAY OFF");
        jl.setFont(new Font(Font.SANS_SERIF,Font.BOLD,30));
        jl.setHorizontalAlignment(JLabel.CENTER);
        add(jl);
        setVisible(true);
    }

    public void getBestPlayersArr(){
        for(SortedPanel sp : sortedPanels){
            bestPlayers.add(sp.getBestPLayer());
        }
    }

    public void update(){
        for(SortedPanel sp : sortedPanels){
            sp.update();
        }
    }

}
