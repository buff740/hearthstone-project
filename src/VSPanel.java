import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class VSPanel extends JPanel {
    private ArrayList<Player> playersArrShuffled;
    ArrayList<PvP> pvpArrayList;
    ArrayList<Player> players;
    int INDEX;

    VSPanel(ArrayList<Player> playersArr, int INDEX){

        this.INDEX = INDEX;

        players = new ArrayList<>();
        pvpArrayList = new ArrayList<>();

        setLayout(new GridLayout(4,1));
        setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
        playersArrShuffled = playersArr;

        makePvPs();

    }

    public void makePvPs(){
        int temp = 0;
        for(int I = INDEX; I<INDEX+8; I++) {
            pvpArrayList.add(new PvP(playersArrShuffled.get(I),playersArrShuffled.get(I+1)));
            players.add(playersArrShuffled.get(I));
            players.add(playersArrShuffled.get(I+1));
            add(pvpArrayList.get(temp));
            temp++;
            I=I+1;
        }
    }

    public void updatePvP(){

        for(PvP p : pvpArrayList){
            p.updateScore();
        }

    }

    public void updateArray(){
        removeAll();
        Collections.sort(players, Collections.reverseOrder());
        pvpArrayList.clear();

        int temp1 = 0;
        int temp2 = 0;
        for(int I = INDEX; I<INDEX+8; I++) {
            pvpArrayList.add(new PvP(players.get(temp1), players.get(temp1+1)));
            add(pvpArrayList.get(temp2));

            temp1+=2;

            temp2+=1;

            I=I+1;
        }

    }

}
