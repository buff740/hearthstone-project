import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

public class SpiderPanel extends JPanel implements ActionListener {
    JButton[] jButtons;
    JLabel fst, snd, trd;
    JButton ok;
    JLabel I,II,III;
    ArrayList<Player> bestPlayers;
    GameWindow parent;
    int PLAYERSNO;

    SpiderPanel(int playersno, ArrayList<Player> bestPlayers, GameWindow parent){

        PLAYERSNO = playersno;
        setLayout(null);
        this.bestPlayers = bestPlayers;
        this.parent = parent;

        try {
            // Set System L&F
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        }
        catch (UnsupportedLookAndFeelException e) {
            // handle exception
        }
        catch (ClassNotFoundException e) {
            // handle exception
        }
        catch (InstantiationException e) {
            // handle exception
        }
        catch (IllegalAccessException e) {
            // handle exception
        }

//        setBackground(Color.GRAY);

        Font f = new Font(Font.SANS_SERIF, Font.BOLD,20);

        fst = new JLabel();
        fst.setFont(f);
        fst.setHorizontalAlignment(JLabel.CENTER);
        fst.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        fst.setBounds(450,260,150,50);

        snd = new JLabel();
        snd.setFont(f);
        snd.setHorizontalAlignment(JLabel.CENTER);
        snd.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        snd.setBounds(700,260,150,50);

        trd = new JLabel();
        trd.setFont(f);
        trd.setHorizontalAlignment(JLabel.CENTER);
        trd.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        trd.setBounds(600,440,150,50);


        I = new JLabel("I.");
        I.setFont(f);
        I.setHorizontalAlignment(JLabel.CENTER);
        I.setBounds(fst.getX(), fst.getY()-fst.getHeight(),150,50);

        II = new JLabel("II.");
        II.setFont(f);
        II.setHorizontalAlignment(JLabel.CENTER);
        II.setBounds(snd.getX(), snd.getY()-snd.getHeight(),150,50);

        III = new JLabel("III.");
        III.setFont(f);
        III.setHorizontalAlignment(JLabel.CENTER);
        III.setBounds(trd.getX(), trd.getY()-trd.getHeight(),150,50);

        ok = new JButton("Ok");
        ok.setBounds(730,50,80,30);
        ok.addActionListener(this);

        add(ok);

        add(I);
        add(II);
        add(III);

        add(fst);
        add(snd);
        add(trd);

        if(playersno==64){

            jButtons = new JButton[16];
            int y = 100;
            for(int I = 0; I < 8; I++){
                jButtons[I] = new JButton(bestPlayers.get(I).toString());
                jButtons[I].setFocusPainted(false);
                jButtons[I].setBounds(50,y,80,30);
                jButtons[I].addActionListener(this);
                y+=50;
                add(jButtons[I]);
            }

            y = 130;

            for(int I = 8; I < 12; I++){
                if(I<=9) {
                    jButtons[I] = new JButton();
                    jButtons[I].setFocusPainted(false);
                    jButtons[I].setBounds(150, y, 80, 30);
                    jButtons[I].addActionListener(this);
                    y += 100;
                    add(jButtons[I]);
                }else{
                    if(y==185){
                        y+=100;
                    }
                    jButtons[I] = new JButton();
                    jButtons[I].setFocusPainted(false);
                    jButtons[I].addActionListener(this);
                    jButtons[I].setBounds(150, y, 80, 30);
                    y += 100;
                    add(jButtons[I]);
                }
            }

            y-=350;

            jButtons[12] = new JButton();
            jButtons[12].setFocusPainted(false);
            jButtons[12].setBounds(250,y,80,30);
            add(jButtons[12]);
            jButtons[12].addActionListener(this);

            jButtons[13] = new JButton();
            jButtons[13].setFocusPainted(false);
            jButtons[13].setBounds(250,y+200,80,30);
            add(jButtons[13]);
            jButtons[13].addActionListener(this);

            jButtons[14] = new JButton();
            jButtons[14].setFocusPainted(false);
            jButtons[14].setBounds(500,400,80,30);
            add(jButtons[14]);
            jButtons[14].addActionListener(this);

            jButtons[15] = new JButton();
            jButtons[15].setFocusPainted(false);
            jButtons[15].setBounds(500,500,80,30);
            add(jButtons[15]);
            jButtons[15].addActionListener(this);

        }else{
            jButtons = new JButton[8];
            int y = 300;
            for(int I = 0; I < 4; I++){
                jButtons[I] = new JButton(bestPlayers.get(I).toString());
                jButtons[I].setFocusPainted(false);
                jButtons[I].setBounds(50,y,80,30);
                jButtons[I].addActionListener(this);
                y+=50;
                add(jButtons[I]);
            }

            y = 330;

            for(int I = 4; I < 6; I++){
                if(I<=5) {
                    jButtons[I] = new JButton();
                    jButtons[I].setFocusPainted(false);
                    jButtons[I].setBounds(150, y, 80, 30);
                    jButtons[I].addActionListener(this);
                    y += 100;
                    add(jButtons[I]);
                }else{
                    if(y==185){
                        y+=100;
                    }
                    jButtons[I] = new JButton();
                    jButtons[I].setFocusPainted(false);
                    jButtons[I].addActionListener(this);
                    jButtons[I].setBounds(150, y, 80, 30);
                    y += 100;
                    add(jButtons[I]);
                }
            }

            jButtons[6] = new JButton();
            jButtons[6].setFocusPainted(false);
            jButtons[6].setBounds(500,400,80,30);
            add(jButtons[6]);
            jButtons[6].addActionListener(this);

            jButtons[7] = new JButton();
            jButtons[7].setFocusPainted(false);
            jButtons[7].setBounds(500,500,80,30);
            add(jButtons[7]);
            jButtons[7].addActionListener(this);
        }

    }

    public void setColor(int i1, int i2){
        if(jButtons[i1].getText()!=""&&jButtons[i2].getText()!="") {
            jButtons[i2].setBackground(Color.RED);
            jButtons[i1].setBackground(Color.GREEN);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(getWidth());
        System.out.println(getHeight());
        if (PLAYERSNO == 64) {
            if (e.getSource() == jButtons[0]) {
                setColor(0, 1);
                jButtons[8].setText(jButtons[0].getText());

            } else if (e.getSource() == jButtons[1]) {
                setColor(1, 0);
                jButtons[8].setText(jButtons[1].getText());

            } else if (e.getSource() == jButtons[2]) {
                setColor(2, 3);
                jButtons[9].setText(jButtons[2].getText());

            } else if (e.getSource() == jButtons[3]) {
                setColor(3, 2);
                jButtons[9].setText(jButtons[3].getText());

            } else if (e.getSource() == jButtons[4]) {
                setColor(4, 5);
                jButtons[10].setText(jButtons[4].getText());

            } else if (e.getSource() == jButtons[5]) {
                setColor(5, 4);
                jButtons[10].setText(jButtons[5].getText());

            } else if (e.getSource() == jButtons[6]) {
                setColor(6, 7);
                jButtons[11].setText(jButtons[6].getText());

            } else if (e.getSource() == jButtons[7]) {
                setColor(7, 6);
                jButtons[11].setText(jButtons[7].getText());

            } else if (e.getSource() == jButtons[8]) {
                setColor(8, 9);
                jButtons[14].setText(jButtons[9].getText());
                jButtons[12].setText(jButtons[8].getText());

            } else if (e.getSource() == jButtons[9]) {
                setColor(9, 8);
                jButtons[12].setText(jButtons[9].getText());
                jButtons[14].setText(jButtons[8].getText());

            } else if (e.getSource() == jButtons[10]) {
                setColor(10, 11);
                jButtons[13].setText(jButtons[10].getText());
                jButtons[15].setText(jButtons[11].getText());

            } else if (e.getSource() == jButtons[11]) {
                setColor(11, 10);
                jButtons[13].setText(jButtons[11].getText());
                jButtons[15].setText(jButtons[10].getText());

            } else if (e.getSource() == jButtons[12]) {
                setColor(12, 13);
                fst.setText(jButtons[12].getText());
                snd.setText(jButtons[13].getText());

            } else if (e.getSource() == jButtons[13]) {
                setColor(13, 12);
                fst.setText(jButtons[13].getText());
                snd.setText(jButtons[12].getText());

            } else if (e.getSource() == jButtons[14]) {
                setColor(14, 15);
                trd.setText(jButtons[14].getText());

            } else if (e.getSource() == jButtons[15]) {
                setColor(15, 14);
                trd.setText(jButtons[15].getText());

            } else if (e.getSource() == ok) {
                parent.end(64);

            }
        }else if(PLAYERSNO==32){
            if (e.getSource() == jButtons[0]) {
                setColor(0, 1);
                jButtons[4].setText(jButtons[0].getText());
                jButtons[6].setText(jButtons[1].getText());

            } else if (e.getSource() == jButtons[1]) {
                setColor(1, 0);
                jButtons[4].setText(jButtons[1].getText());
                jButtons[6].setText(jButtons[0].getText());

            } else if (e.getSource() == jButtons[2]) {
                setColor(2, 3);
                jButtons[5].setText(jButtons[2].getText());
                jButtons[7].setText(jButtons[3].getText());

            } else if (e.getSource() == jButtons[3]) {
                setColor(3, 2);
                jButtons[5].setText(jButtons[3].getText());
                jButtons[7].setText(jButtons[2].getText());

            } else if(e.getSource()==jButtons[4]){
                setColor(4, 5);
                fst.setText(jButtons[4].getText());
                snd.setText(jButtons[5].getText());

            } else if(e.getSource()==jButtons[5]){
                setColor(5, 4);
                fst.setText(jButtons[5].getText());
                snd.setText(jButtons[4].getText());

            } else if(e.getSource()==jButtons[6]){
                setColor(6, 7);
                trd.setText(jButtons[6].getText());

            } else if(e.getSource()==jButtons[7]){
                setColor(7, 6);
                trd.setText(jButtons[7].getText());

            }else if(e.getSource()==ok){
                parent.end(32);

            }
        }
    }
}
