import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameWindow extends JFrame implements ActionListener {
    static int times;
    static int PLAYERSNO, ROUNDSNO;
    private ArrayList<Player> playerArr;
    public JPanel top, bottom, main;
    public PreGamePanel preGamePanel;
    public JButton start, continueb;
    private Boolean started = false;
    private TimerPanel timerPanel;
    private Boolean playoffb = false;
    private SpiderPanel sp;
    ArrayList<VSPanel> vsPanels;
    ImagePanel[] imagePanels;

    GameWindow(int PLAYERSNO, int ROUNDSNO, List<Player> playerArr){

        setTitle("Hearthstone tournament");
        imagePanels = new ImagePanel[6];
        imagePanels[0] = new ImagePanel(480, 140, "avast.png");
        imagePanels[1] = new ImagePanel(480, 140, "gympl.png");
        imagePanels[2] = new ImagePanel(480, 140, "nejlevpc.png");
        imagePanels[3] = new ImagePanel(480, 140, "plzenkraj.png");
        imagePanels[4] = new ImagePanel(480, 565, "ponozky.png");
        imagePanels[5] = new ImagePanel(450, 224, "hslogo.png");

        setSize(1920,1080);
        setExtendedState(MAXIMIZED_BOTH);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);

        imagePanels[0].setLocation(0,0);
        imagePanels[1].setLocation(480,0);
        imagePanels[2].setLocation(960,0);
        imagePanels[3].setLocation(1440,0);
        imagePanels[4].setLocation(1440,140);
        imagePanels[5].setLocation(1455,750);

        add(imagePanels[0]);
        add(imagePanels[1]);
        add(imagePanels[2]);
        add(imagePanels[3]);
        add(imagePanels[4]);
        add(imagePanels[5]);


        main = new JPanel();
        main.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        main.setBounds(0,140,1440,885);
        main.setLayout(new BorderLayout());

        vsPanels = new ArrayList<>();

        top = new JPanel();
        top.setLayout(new GridLayout(2,4,20,20));

        bottom = new JPanel();
        bottom.setLayout(new GridLayout(1,3));

        this.playerArr = new ArrayList<Player>(playerArr);

        Collections.shuffle(this.playerArr);

        this.PLAYERSNO = PLAYERSNO;
        this.ROUNDSNO = ROUNDSNO;

        preGamePanel = new PreGamePanel(PLAYERSNO, this.playerArr);

        main.add(preGamePanel, BorderLayout.CENTER);
        main.add(bottom, BorderLayout.SOUTH);

        add(main);

        timerPanel = new TimerPanel(90,this);

        continueb = new JButton("OK");
        continueb.setBackground(Color.DARK_GRAY);
        continueb.addActionListener(this);
        continueb.setEnabled(false);
        continueb.setFocusPainted(false);

        start = new JButton("Ok");
        start.setFocusPainted(false);
        start.addActionListener(this);
        start.setBackground(Color.DARK_GRAY);

        bottom.add(timerPanel);
        bottom.add(start);


        timerPanel.setVisible(false);

    }

    public void makeVSPanels(){
        int temp = 0;
        for(int I = 0; I<PLAYERSNO/8; I++){
            vsPanels.add(new VSPanel(playerArr,temp));
            top.add(vsPanels.get(I));
            temp+=8;
        }
    }

    public void updateVSPanels(){
        for(VSPanel v : vsPanels){
            v.updatePvP();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource()==start){
            if(!started) {
                timerPanel.setVisible(true);
                start.setText("Start");
                started = true;
                preGamePanel.setVisible(false);
                main.add(top, BorderLayout.CENTER);
                makeVSPanels();
//                preGamePanel.printwh();
            }else{
                new Thread(timerPanel).start();
                start.setEnabled(false);
                bottom.remove(start);
                bottom.add(continueb);

            }
        }else if(e.getSource() == continueb) {
            if (!playoffb) {
                if (times == 3) {

                    preGamePanel.playOff();
                    playoffb = true;

                } else {
                    for (VSPanel v : vsPanels) {
                        v.updateArray();
                    }
                    new Thread(timerPanel).start();

                    continueb.setEnabled(false);
                    preGamePanel.setVisible(false);
                    top.setVisible(true);
                }
            }else if(playoffb){
                timerPanel.setVisible(false);
                continueb.setVisible(false);
                top.setVisible(false);
                preGamePanel.setVisible(false);
                main.remove(preGamePanel);
                sp = new SpiderPanel(PLAYERSNO, preGamePanel.bestPlayers,this);
                main.add(sp, BorderLayout.CENTER);

            }
        }

    }

    public void end(int playersno){
        bottom.setVisible(false);
        sp.setVisible(false);
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));

        sp.I.setHorizontalAlignment(JLabel.RIGHT);
        sp.fst.setHorizontalAlignment(JLabel.LEFT);
        sp.II.setHorizontalAlignment(JLabel.RIGHT);
        sp.snd.setHorizontalAlignment(JLabel.LEFT);
        sp.III.setHorizontalAlignment(JLabel.RIGHT);
        sp.trd.setHorizontalAlignment(JLabel.LEFT);

        panel.add(sp.I);
        sp.fst.setBorder(null);
        panel.add(sp.fst);
        panel.add(sp.II);
        panel.add(sp.snd);
        sp.snd.setBorder(null);
        panel.add(sp.III);
        panel.add(sp.trd);
        sp.trd.setBorder(null);
        main.add(panel);
    }

}
