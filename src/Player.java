


public class Player implements Comparable<Player>{
    private String name;
    private int SCORE;
    public int GAMESLOST, GAMESWON;
    private int PS;

    Player(String name){
        this.name = name;
        SCORE = 0;
    }

    public void setPS(int PS){
        this.PS = PS;
    }

    public String toString(){
        return name;
    }

    public String getWL(){
        return GAMESWON + " - " + GAMESLOST;
    }

    public void setSCORE(int newSCORE){
        SCORE = newSCORE;
    }


    public int getSCORE(){
        return SCORE;
    }

    @Override
    public int compareTo(Player o) {
        return this.SCORE - o.SCORE;
    }

}
