import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class SortedPanel extends JPanel{
    public ArrayList <Player> playersArr;
    private int INDEX;
    Graphics g;
    BufferedImage image;
    int size;
    static Boolean playoff = false;

    SortedPanel(int INDEX, ArrayList <Player> playersArr, int rows, int cols, int pom){
        g = getGraphics();
        size = playersArr.size();

        try {
            image = ImageIO.read(ImagePanel.class.getResource("/images/hspapyrus2.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.INDEX = INDEX;
        this.playersArr = new ArrayList<Player>();

        setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));

        if(pom==8){
            for(int I = 0; I < pom; I++){
                this.playersArr.add(playersArr.get(I));
            }
        }else if(pom==4){
            for(int I = 0; I < pom; I++){
                this.playersArr.add(playersArr.get(I));
            }
        } else {
            for (int I = this.INDEX; I < this.INDEX + 8; I++) {
                this.playersArr.add(playersArr.get(I));
            }
        }

        setLayout(new GridLayout(rows,cols));

        for(Player p : this.playersArr){
            JLabel l1 = new JLabel(p.toString(), JLabel.CENTER);
            l1.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));
            JLabel l2 = new JLabel(p.getWL(), JLabel.CENTER);
            l2.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));

            add(l1);
            add(l2);
        }

    }

    public void paintComponent(Graphics g){
        if(!playoff) {
            super.paintComponent(g);
            if (size == 32) {
                g.drawImage(image, 0, 0, 709, 420, null);
            } else {
                g.drawImage(image, 0, 0, 344, 420, null);
            }
        }
    }

    public Player getBestPLayer(){
        return playersArr.get(0);
    }

    public void update(){
        Collections.sort(this.playersArr, Collections.reverseOrder());

        this.removeAll();
//        for(Player p : this.playersArr){
        for(int I = 0; I < playersArr.size(); I++){

//            JLabel l1 = new JLabel(p.toString(), JLabel.CENTER);
            JLabel l1 = new JLabel(I+1 + ". " + playersArr.get(I).toString(), JLabel.CENTER);
            l1.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));

//            JLabel l2 = new JLabel(p.getWL(), JLabel.CENTER);
            JLabel l2 = new JLabel(playersArr.get(I).getWL(), JLabel.CENTER);
            l2.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));

            add(l1);
            add(l2);
        }
    }

}
