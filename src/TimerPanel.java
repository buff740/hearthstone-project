import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TimerPanel extends JPanel implements Runnable, ActionListener {
    private int minutes, x;
    private JLabel label;
    private JButton button;
    private GameWindow parent;
    Boolean ended = false;

    TimerPanel(int minutes, GameWindow parent){

        this.parent = parent;
        setLayout(new GridLayout(1,2));
        setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
        this.minutes = minutes * 60;

        button = new JButton("stop");
        button.addActionListener(this);

        label = new JLabel();
        label.setFont(new Font(Font.SANS_SERIF,Font.BOLD,15));
        label.setText(this.minutes/60 + ":" + this.minutes%60 + "0");
        label.setHorizontalAlignment(JLabel.CENTER);
        add(label);
        add(button);

    }

    @Override
    public void run() {
        ended = false;
        x = this.minutes;
        while(true) {

            if(x == -1){
                if(ended) {
                    parent.start.setText("Pokracovat");
                    parent.start.setEnabled(true);
                    parent.top.setVisible(false);
                    parent.updateVSPanels();
                    parent.continueb.setEnabled(true);
                    parent.preGamePanel.update();
                    parent.preGamePanel.setVisible(true);
                    parent.times++;
                    break;
                }else if(!ended){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }

            }

            if(x%60 < 10) {
                label.setText(x / 60 + ":" + "0" + x % 60);
            }else{
                label.setText(x / 60 + ":" + x % 60);
            }

            x-=1;

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        x = -1;
        ended = true;
    }
}
