import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ImagePanel extends JPanel {
    private BufferedImage image;
    Graphics g;
    String imagename;
    int width,height;

    ImagePanel(int width, int height, String imagename){
        this.width = width;
        this.height = height;
        this.imagename = imagename;
//        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setSize(width, height);
        g = getGraphics();
        loadImage();
    }

    public void loadImage(){
        try {
            image = ImageIO.read(ImagePanel.class.getResource("/images/" + "resized/" + imagename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.drawImage(image,0,0, width, height,null);
    }

}
